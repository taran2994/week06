import java.util.ArrayList;

public class player {

	private String name;
	private int money;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public ArrayList<Pokemon> getTeam() {
		return team;
	}
	public void setTeam(ArrayList<Pokemon> team) {
		this.team = team;
	}
	public player(String name, int money) {
		super();
		this.name = name;
		this.money = money;
	}
	private ArrayList<Pokemon> team= new ArrayList<Pokemon>();
	 public void addPokemon(Pokemon poke)
	 {
		 this.team.add(poke);
	 }
	 public void printTeam()
	 {  for(int i=0;i<team.size();i++){
		 System.out.println(this.team.get(i).getName());
	 }
	 }
}
